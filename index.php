<?php include '/core/config.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Student Activities Attendance System</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <!-- <link rel="icon" type="image/png" href="assets/images/icons/favicon.ico"/> -->
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="assets/css/util.css">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
<!--===============================================================================================-->
</head>
<body>
  
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100 p-t-20 p-b-30">
        <div class="text-center row">
          <div class="alert alert-secondary alert-dismissible fade show col-md-12 row" role="alert" style="display:none;">
            <div id="notif" class="col-md-10 p-l-5 p-t-10">
              
            </div>
            <button type="button" class="close float-right" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        <form id="form_login" class="login100-form validate-form">

          <span class="h2 p-t-20 p-b-45 text-center text-white">
            Students Activities Attendance System <br><small>with</small><br>Facial Recognition
          </span>

          <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
            <input class="input100" type="text" name="username" id="uname" placeholder="Username">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
            <input class="input100" type="password" name="password" id="pass" placeholder="Password">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock"></i>
            </span>
          </div>

          <div class="container-login100-form-btn p-t-10">
            <button type="submit" class="login100-form-btn btn-login">
              Login
            </button>
          </div>

         <!--  <div class="text-center w-full p-t-25">
            <a href="#" class="txt1">
              Forgot Username / Password?
            </a>
          </div> -->
        </form>
      </div>
    </div>
  </div>
  
  

  
<!--===============================================================================================-->  
  <script src="assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="assets/vendor/bootstrap/js/popper.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
    $(document).ready( function(){
      $("#form_login").on("submit",function(e){
          e.preventDefault();
          var data = $(this).serialize();
          var uname = $("#uname").val();
          var pass = $("#pass").val();
          if(uname != "" && pass != ""){
            $.ajax({
              "type"  :"POST",
              "url"   :"ajax/auth.php",
              "data"  : data,
              "success": function(data)
              {
                $(".btn-login").prop("disabled", true);
                $(".alert").slideDown();
                $("#notif").html('<strong>'+
                '<i class="fa fa-refresh fa-spin"></i> Processing'+
              '</strong>'+
              ', Please wait...');
                setTimeout( function(){
                  // alert("goods!");
                  if(data == 1){
                    // $(".alert-info").removeClass("alert-info");
                    $("#notif").addClass("text-success p-t-10");
                    $("#notif").html('<strong>'+
                    '<i class="fa fa-check-circle"></i> Success!'+
                  '</strong>'+
                  ' Signing in, Please wait...');
                    $(".btn-login").prop("disabled", true);
                    setTimeout( function(){
                      $(".alert").slideUp();
                      $("#notif").removeClass("text-success p-t-10");
                      setTimeout( function(){
                        window.location="auth/index.php?page=<?php echo page_url('dashboard')?>";
                      },500);
                    },1500);
                  }else if(data == 2){
                    // $(".alert-info").removeClass("alert-info");
                    $("#notif").addClass("text-warning");
                    $("#notif").html('<strong>'+
                    '<i class="fa fa-exclamation-circle"></i> Warning!'+
                  '</strong>'+
                  ' Account was disabled. Please contact administrator.');
                    $(".btn-login").prop("disabled", false);
                    setTimeout( function(){
                        $(".alert").slideUp();
                        $("#notif").removeClass("text-warning");
                      },1500);
                  }else{
                    // $(".alert-info").removeClass("alert-info");
                    $("#notif").addClass("text-danger p-t-10");
                    $("#notif").html('<strong>'+
                    '<i class="fa fa-times-circle"></i> Error!'+
                  '</strong>'+
                  ' Wrong Credentials');
                    $(".btn-login").prop("disabled", false);
                    setTimeout( function(){
                        $(".alert").slideUp();
                        $("#notif").removeClass("text-danger p-t-10");
                      },1500);
                  }
                },1500);
              }
            });
          }
      });
    });
  </script>
</body>
</html>