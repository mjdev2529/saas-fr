<!-- Modal -->
<div class="modal fade" id="add_event_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-calendar-alt mr-2"></i> Add New Activity/Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="addEvent_form" class="form-row">
            <div class="col-md-12">
              <div class="row">
                <div class="form-group col-md-5">
                  <label for="inputEmail4">Name</label>
                  <input type="text" class="form-control" id="ename" name="name" placeholder="Name" required="">
                </div>
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Comittee</label>
                  <select id="ecommitte" class="form-control fSelect" multiple="">
                      <?php
                        $comitteeSql = mysql_query("SELECT * FROM tbl_users WHERE urole = 1");
                        while ($row = mysql_fetch_array($comitteeSql)) { ?>
                        <option value="><?php echo $row[user_id]?>"><?php echo $row["u_lname"].", ".$row["u_fname"]." ".$row["u_mname"].".";?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Date</label>
                  <input type="date" class="form-control" id="edate" name="date" placeholder="Date" required="">
                </div>
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Time Start</label>
                  <input type="time" class="form-control" id="etimeS" name="timeS" placeholder="Time Start" required="">
                </div>
                <div class="form-group col-md-4">
                  <label for="inputEmail4">Time End</label>
                  <input type="time" class="form-control" id="etimeE" name="timeE" placeholder="Time End" required="">
                </div>
                <div class="form-group col-md-12">
                  <label for="inputPassword4">Details</label>
                  <textarea class="form-control" cols="2" placeholder="Type Here..." id="edetails" name="details" required="" maxlength="200"></textarea>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-success offset-md-10">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>