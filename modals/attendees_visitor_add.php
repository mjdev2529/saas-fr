<!-- Modal -->
<div class="modal fade" id="md_attendees_visitor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-plus mr-2"></i> Add New Visitor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="visitorAdd_form" class="form-row">
            <div class="col-md-8 offset-md-2">
              <div class="form-group">
                <label for="inputEmail4">First Name</label>
                <input type="text" class="form-control" id="v_fname" name="fname" placeholder="First Name" required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Last Name</label>
                <input type="text" class="form-control" id="v_lname" name="lname" placeholder="Last Name" required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Contact No.</label>
                <input type="text" class="form-control" id="v_contact_num" name="contact_num" placeholder="Contact No." required="">
              </div>
              <div class="form-group col-md-12">
                <label for="inputPassword4">Address</label>
                <textarea class="form-control" cols="2" placeholder="Type Here..." id="v_address" name="address" required="" maxlength="200"></textarea>
              </div>
            </div>
            <button type="submit" class="btn btn-success offset-md-9">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>