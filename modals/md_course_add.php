<!-- Modal -->
<div class="modal fade" id="md_course_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-clipboard mr-2"></i> Add New Course</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="courseAdd_form" class="form-row">
            <div class="col-md-12">
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="inputEmail4">Course Name</label>
                  <input type="text" class="form-control" id="ename" name="name" placeholder="Course Name" required="">
                </div>
            </div>
            <button type="submit" class="btn btn-success btn-block">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>