<!-- Modal -->
<div class="modal fade" id="md_course_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit mr-2"></i> Edit Course</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="courseEdit_form" class="form-row" action="" method="POST">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputEmail4">Course Name</label>
                <input type="text" class="form-control" id="cname" name="name" placeholder="Course Name">
                <input type="hidden" name="c_id" id="c_id">
              </div>
            </div>
            <button type="submit" class="btn btn-success btn-block">Save Changes</button>
          </form>
      </div>
    </div>
  </div>
</div>