<!-- Modal -->
<div class="modal fade" id="md_student_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-edit mr-2"></i> Edit Student</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="studentEdit_form" class="form-row">
            <input type="hidden" id="a_id" name="attendee_id">
            <div class="col-md-8 offset-md-2">
              <div class="form-group">
                <label for="inputEmail4">Student ID No.</label>
                <input type="text" class="form-control" id="edit_s_id_num" name="s_id_num" placeholder="Student ID No." required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">First Name</label>
                <input type="text" class="form-control" id="edit_s_fname" name="fname" placeholder="First Name" required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Last Name</label>
                <input type="text" class="form-control" id="edit_s_lname" name="lname" placeholder="Last Name" required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Course</label>
                <select id="edit_s_course" name="course" class="form-control">
                  <option value="0">Select Course:</option>
                    <?php
                      $courseSql = mysql_query("SELECT * FROM tbl_course");
                      while ($row = mysql_fetch_array($courseSql)) { ?>
                      <option value="<?php echo $row['course_id']?>"><?php echo $row["course_name"];?></option>
                    <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="inputEmail4">Year</label>
                <select id="edit_s_year" class="form-control" name="year">
                  <option value="0">Select Year:</option>
                  <option value="1">I</option>
                  <option value="2">II</option>
                  <option value="3">III</option>
                  <option value="4">IV</option>
                  <option value="5">V</option>
                  <option value="6">IRREGULAR</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputEmail4">Section</label>
                <input type="text" class="form-control" id="edit_s_section" name="section" placeholder="Section" required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Contact No.</label>
                <input type="text" class="form-control" id="edit_s_contact_num" name="contact_num" placeholder="Contact No." required="">
              </div>
              <div class="form-group col-md-12">
                <label for="inputPassword4">Address</label>
                <textarea class="form-control" cols="2" placeholder="Type Here..." id="edit_s_address" name="address" required="" maxlength="200"></textarea>
              </div>
            </div>
            <button type="submit" class="btn btn-success offset-md-9">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>