<!-- Modal -->
<div class="modal fade" id="md_visitor_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-edit mr-2"></i> Edit Visitor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="visitorEdit_form" class="form-row">
            <input type="hidden" id="a_Vid" name="attendee_id">
            <div class="col-md-8 offset-md-2">
              <div class="form-group">
                <label for="inputEmail4">First Name</label>
                <input type="text" class="form-control" id="edit_fname" name="fname" placeholder="First Name" required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Last Name</label>
                <input type="text" class="form-control" id="edit_lname" name="lname" placeholder="Last Name" required="">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Contact No.</label>
                <input type="text" class="form-control" id="edit_contact_num" name="contact_num" placeholder="Contact No." required="">
              </div>
              <div class="form-group col-md-12">
                <label for="inputPassword4">Address</label>
                <textarea class="form-control" cols="2" placeholder="Type Here..." id="edit_address" name="address" required="" maxlength="200"></textarea>
              </div>
            </div>
            <button type="submit" class="btn btn-success offset-md-9">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>