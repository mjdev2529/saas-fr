<!-- Modal -->
<div class="modal fade" id="edit_event_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> Edit Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="editEvent_form" class="form-row">
            <div class="col-md-12">
              <div class="row">
                <div class="form-group col-md-4 offset-md-8">
                  <label for="inputEmail4">Event Status</label>
                  <select id="estatus" name="estatus" class="ml-2">
                    <option value="0">Please Choose:</option>
                    <option value="1">Finished</option>
                    <option value="2">Cancel</option>
                  </select>
                </div>
                <div class="form-group col-md-3">
                  <label for="inputEmail4">Name</label>
                  <input type="text" class="form-control" id="e_ename" name="name" placeholder="Name" required="">
                  <input type="hidden" id="e_id" name="e_id">
                </div>
                <div class="form-group col-md-3">
                  <label for="inputEmail4">Date</label>
                  <input type="date" class="form-control" id="e_edate" name="date" placeholder="Date" required="">
                </div>
                <div class="form-group col-md-3">
                  <label for="inputEmail4">Time Start</label>
                  <input type="time" class="form-control" id="e_etimeS" name="timeS" placeholder="Time Start" required="">
                </div>
                <div class="form-group col-md-3">
                  <label for="inputEmail4">Time End</label>
                  <input type="time" class="form-control" id="e_etimeE" name="timeE" placeholder="Time End" required="">
                </div>
                <div class="form-group col-md-12">
                  <label for="inputPassword4">Details</label>
                  <textarea class="form-control" cols="2" placeholder="Type Here..." id="e_edetails" name="details" required="" maxlength="200"></textarea>
                </div>
              </div>

              <!-- COMMITEE -->
              <hr>
              <div class="row mb-3">
                <label for="inputPassword4">Committee:</label>
                <div class="col-md-4 row ml-1">
                  <select id="e_ecommitte" class="form-control fSelect" multiple="" name="ecommitte[]">
                      <?php
                        $comitteeSql = mysql_query("SELECT * FROM tbl_users WHERE (urole = 1 OR urole = 2)");
                        while ($row = mysql_fetch_array($comitteeSql)) {
                          $mname = $row["u_mname"]!= ""?" ".$row["u_mname"].".":"";
                         ?>
                        <option value="<?php echo $row['user_id']?>"><?php echo $row["u_lname"].", ".$row["u_fname"].$mname;?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-md-2">
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-sm" onclick="addCommittee()"><i class="fa fa-plus"></i> Add</button>
                  </div>
                </div>
                <div class="col-md-12 mt-2">
                  <div class="table-responsive">
                    <table width="100%" id="tbl_committee" class="table m-0 table-bordered">
                      <thead class="bg-dark">
                      <tr>
                        <th class="w-5"></th>
                        <th class="w-5">#</th>
                        <th>Committe</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
            <button type="submit" class="btn btn-success offset-md-10">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>