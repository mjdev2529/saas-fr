<!-- Modal -->
<div class="modal fade" id="add_user_md" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-plus mr-2"></i> Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="addUser_form" class="form-row" action="" method="POST">
            <div class="col-md-8 offset-md-2">
              <div class="form-group">
                <label for="inputEmail4">First Name</label>
                <input type="text" class="form-control" id="ufname" name="fname" placeholder="First Name">
              </div>
              <div class="form-group">
                <label for="inputPassword4">Middle Initial</label>
                <input type="text" class="form-control" id="umname" name="mname" placeholder="Middle Initial" maxlength="1">
              </div>
              <div class="form-group">
                <label for="inputPassword4">Last Name</label>
                <input type="text" class="form-control" id="ulname" name="lname" placeholder="Last Name">
              </div>
              <div class="form-group">
                <label for="inputAddress">Username</label>
                <input type="text" class="form-control" id="uname" name="uname" placeholder="Username">
              </div>
              <div class="form-group">
                <label for="inputAddress2">Password</label>
                <input type="password" class="form-control" id="upass" name="pass" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="inputState">Role</label>
                <select id="inputState" class="form-control" name="role">
                  <option selected value="0">Please choose role:</option>
                  <option value="1">Comittee</option>
                  <option value="2">Guard</option>
                </select>
              </div>
            </div>
            <button type="submit" class="btn btn-success btn-block offset-md-3 col-md-6">Save</button>
          </form>
      </div>
    </div>
  </div>
</div>