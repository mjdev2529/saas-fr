-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2020 at 08:59 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saas_fr`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendees`
--

CREATE TABLE `tbl_attendees` (
  `attendee_id` int(11) NOT NULL,
  `attendee_type` text NOT NULL,
  `student_id` varchar(200) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `course_id` int(11) NOT NULL,
  `student_year` int(11) NOT NULL,
  `student_section` varchar(11) NOT NULL,
  `contact_num` varchar(11) NOT NULL,
  `address` varchar(200) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attendees`
--

INSERT INTO `tbl_attendees` (`attendee_id`, `attendee_type`, `student_id`, `fname`, `lname`, `course_id`, `student_year`, `student_section`, `contact_num`, `address`, `added_by`, `date_added`) VALUES
(4, 'S', 'test10000', 'test', 'test', 6, 1, 'A', '0912345789', 'test', 1, '2020-07-03'),
(5, 'V', '', 'test', 'test', 0, 0, '', '0912345789', 'test', 1, '2020-07-03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_committee`
--

CREATE TABLE `tbl_committee` (
  `committee_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_committee`
--

INSERT INTO `tbl_committee` (`committee_id`, `user_id`, `event_id`, `added_by`, `date_added`) VALUES
(1, 2, 7, 0, '2020-06-14'),
(2, 3, 7, 0, '2020-06-14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(200) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`course_id`, `course_name`, `added_by`, `date_added`) VALUES
(5, 'BSINFO', 1, '2020-06-14'),
(6, 'BSIS', 1, '2020-06-14'),
(7, 'BSIT - FOODTECH', 1, '2020-06-14'),
(8, 'BSIT - COMPTECH', 1, '2020-06-14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_activities`
--

CREATE TABLE `tbl_events_activities` (
  `event_id` int(11) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  `event_time1` time NOT NULL,
  `event_name` varchar(150) NOT NULL,
  `event_details` varchar(200) NOT NULL,
  `event_status` int(11) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_events_activities`
--

INSERT INTO `tbl_events_activities` (`event_id`, `event_date`, `event_time`, `event_time1`, `event_name`, `event_details`, `event_status`, `added_by`) VALUES
(7, '2020-06-14', '08:00:00', '17:00:00', 'test event', 'test', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `u_fname` varchar(100) NOT NULL,
  `u_mname` varchar(100) NOT NULL,
  `u_lname` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `urole` int(11) NOT NULL,
  `ustatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `u_fname`, `u_mname`, `u_lname`, `uname`, `password`, `urole`, `ustatus`) VALUES
(1, 'root', '', 'account', 'root', '12345', -1, -1),
(2, 'Test', '', 'Committe', 'test', '12345', 1, 1),
(3, 'Guard', '', 'Test', 'guard', '12345', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_attendees`
--
ALTER TABLE `tbl_attendees`
  ADD PRIMARY KEY (`attendee_id`);

--
-- Indexes for table `tbl_committee`
--
ALTER TABLE `tbl_committee`
  ADD PRIMARY KEY (`committee_id`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `tbl_events_activities`
--
ALTER TABLE `tbl_events_activities`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_attendees`
--
ALTER TABLE `tbl_attendees`
  MODIFY `attendee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_committee`
--
ALTER TABLE `tbl_committee`
  MODIFY `committee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_events_activities`
--
ALTER TABLE `tbl_events_activities`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
