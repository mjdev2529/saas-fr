<h5 class="text-center pt-3"><i class="fa fa-cogs mr-1"></i> Settings</h5>
<hr class="mb-2"/>
<ul class="nav flex-column">
  <li class="nav-item">
    <a href="index.php?page=<?php echo page_url('profile');?>" class="nav-link">
      <i class="fa fa-user mr-2"></i> Profile
    </a>
  </li>
  <?php if($_SESSION["role"] == "Administrator"){ ?>
    <li class="nav-item">
      <a href="index.php?page=<?php echo page_url('users');?>" class="nav-link">
        <i class="fa fa-users mr-2"></i> Manage Users
      </a>
    </li>
    <li class="nav-item">
      <a href="index.php?page=<?php echo page_url('course');?>" class="nav-link">
        <i class="fa fa-clipboard mr-2"></i> Manage Courses
      </a>
    </li>
  <?php } ?>
  <?php if($_SESSION["role"] != "Administrator"){ ?>
    
  <?php } ?>
  <li class="nav-item">
    <a href="#" class="nav-link" onclick="log_out()">
    	<i class="fa fa-power-off mr-2"></i> Log out
    </a>
  </li>
</ul>