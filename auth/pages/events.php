<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Actvities and Events</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active">Actvities and Events</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <div class="card">
              <div class="card-header border-transparent">
                <div class="btn-group float-sm-right">
                  <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_event_md">Add</button>
                  <button class="btn btn-sm btn-danger" onclick="delete_event()">Delete</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tbl_events" class="table m-0 table-bordered">
                    <thead class="bg-dark">
                    <tr>
                      <th width="5px"><input type="checkbox" id="cb_events" onclick="checkAllEvents()"></th>
                      <th width="5px">#</th>
                      <th width="120px">DATE</th>
                      <th>ACTIVITY</th>
                      <th width="130px">DURATION</th>
                      <th width="50px">STATUS</th>
                      <th width="100px;">ATTENDANCE</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
            </div>

    </section>
    <!-- /.content -->
    <?php
      include "../modals/add_event.php";
      include "../modals/edit_events.php";
    ?>

<script type="text/javascript">
  $(document).ready( function(){
    events_data();
  });

  function events_data(){
    var tbl = $('#tbl_events').DataTable();
   tbl.destroy();
    $('#tbl_events').DataTable({
        "processing": true,
        "ajax": {
            "url": "../ajax/datatables/eventboard.php",
            "type": "POST"
        },
        "columns": [
            { 
                "mRender": function(data,type,row){
                  var x = row.event_status == 1?"style='display:none;'":"";
                  return "<input type='checkbox' "+x+" name='events' value='"+row.event_id+"'>";
              } 
            },
            { "data": "count" },
            // { 
            //     "mRender": function(data,type,row){
            //       return "<button class='btn btn-sm btn-dark' onclick='edit_MD("+row.event_id+")'><i class='fa fa-list'></i></button>";
            //   } 
            // },
          { "data": "event_date" },
            { 
                "mRender": function(data,type,row){
                  return "<u><a href='#' onclick='edit_MD("+row.event_id+")'>"+row.event_name+"</a></u>";
              } 
            },
            { "data": "duration" },
            { "data": "event_status" },
            { "data": "event_attendance" }
        ],
        "createdRow": function ( row, data, dataIndex ) {
            if (data.alert == 1) {
                $(row).css("background-color","#f5dfa2");
            }
        }
    });
  }

  function checkAllEvents(){
    var x = $("#cb_events").is(":checked");

    if(x){
      $("input[name=events]").prop("checked",  true);
    }else{
      $("input[name=events]").prop("checked",  true);
    }
  }

  $("#addEvent_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/add_event.php";

    $.ajax({
      "type": "POST",
      "data": data,
      "url": url,
      "success": function(data){
        if(data == 1){
          $("#add_event_md").modal("hide");
          $("input").val("");
          $("textarea").html("");
          alert("Event added successfully.");
          events_data();
        }else{
          alert("Error!");
        }
      }
    });
  });

  function delete_event(){
    var x = [];
    $("input[name=events]:checked").each( function(){
      x.push($(this).val());
    });

     var url = "../ajax/delete_event.php";

    if(x != ""){
      var z = confirm("Are you sure to delete selected?");
      if(z){
        $.ajax({
          "type": "POST",
          "data": {uID: x},
          "url": url,
          "success": function(data){
            if(data == 1){
              alert("Selected data was successfully deleted.");
              events_data();
            }else{
              alert("Error!");
            }
          }
        });
      }
    }else{
      alert("No data selected!");
    }
  }

  function edit_MD(eID){
    $("#edit_event_md").modal();
    event_details(eID);
    committee_data(eID);
  }

  function event_details(eID){
    var url = "../ajax/event_data.php"
    $.ajax({
      "type": "POST",
      "url": url,
      "data": {eID: eID},
      "success": function(data){
        if(data){
          var o = JSON.parse(data);
          $("#e_ename").val(o.event_name);
          // $("#e_umname").val(o.u_mname);
          $("#e_edate").val(o.event_date);
          $("#e_etimeS").val(o.event_time);
          $("#e_etimeE").val(o.event_time1);
          $("#e_edetails").val(o.event_details);
          $("#estatus").val(o.event_status);
          $("#e_id").val(o.event_id);
        }
      }
    });
  }

  $("#editEvent_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/edit_event.php";
    $.ajax({
      "type": "POST",
      "data": data,
      "url": url,
      "success": function(data){
        if(data == 1){
          $("#edit_event_md").modal("hide");
          alert("Event details was updated successfully.");
          events_data();
        }else{
          alert("Error!");
        }
      }
    });
  });

  function addCommittee(){
    var committee = $("#e_ecommitte").val();
    var eID = $("#e_id").val();
    var url = "../ajax/event_committee_add.php";
    $.ajax({
      "type": "POST",
      "data": {committee: committee, eID: eID},
      "url": url,
      "success": function(data){
        if(data == 1){
          alert("Event committee was added successfully.");
          committee_data(eID);
        }else if(data != 1 && data != 0){
          alert(data);
        }else{
          alert("Error!");
        }
        var x = $("#e_ecommitte > option").is(":checked");
        $(x).prop("checked", false);
      }
    });
  }

  
  function deleteCom(id){
    var url = "../ajax/event_committee_delete.php";
    var x = confirm("Are you sure to delete selected?");
    if(x){
      $.ajax({
        "type": "POST",
        "data": {cID: id},
        "url": url,
        "success": function(data){
          if(data == 1){
            alert("Event committee was updated successfully.");
          }else{
            alert("Error!");
          }
          committee_data(0);
        }
      });
    }
  }

  function committee_data(eID){
    // var eID = $("#e_id").val();
    var tbl = $('#tbl_committee').DataTable();
    tbl.destroy();
    $('#tbl_committee').DataTable({
        "processing": true,
        "ajax": {
            "url": "../ajax/datatables/committee_dt.php",
            "type": "POST",
            "data": {eID: eID}
        },
        "columns": [
            { 
                "mRender": function(data,type,row){
                  return "<button type='button' class='btn btn-sm btn-danger' onclick='deleteCom("+row.committee_id+")'><i class='fa fa-trash'></i></button>";
              } 
            },
            { "data": "count" },
            { "data": "committee_name" },
        ]
    });
  }

</script>