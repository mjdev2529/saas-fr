<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <?php if($_SESSION["role"] == "Administrator"){ ?>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-calendar-alt"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">EVENTS & ACTIVITIES</span>
                  <span class="info-box-number">
                    <?php echo mysql_num_rows(mysql_query("SELECT event_id FROM tbl_events_activities"));?>
                    <!-- <small>%</small> -->
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-list"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">ATTENDANCE</span>
                  <span class="info-box-number"><?php echo mysql_num_rows(mysql_query("SELECT attendee_id FROM tbl_attendees"));?></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-id-card"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">STUDENTS</span>
                  <span class="info-box-number"><?php echo mysql_num_rows(mysql_query("SELECT attendee_id FROM tbl_attendees WHERE attendee_type = 'S'"));?></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">VISITORS</span>
                  <span class="info-box-number"><?php echo mysql_num_rows(mysql_query("SELECT attendee_id FROM tbl_attendees WHERE attendee_type = 'V'"));?></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>

      <?php } ?>

      <div class="card">
        <div class="card-header border-transparent">
          <h3 class="card-title">EVENTS & ACTIVITIES</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="table-responsive">
            <table id="tbl_events" class="table table-bordered">
              <thead>
              <tr class="bg-dark">
                <th width="10px">#</th>
                <th width="150px">DATE</th>
                <th>ACTIVITY</th>
                <th width="50px">STATUS</th>
                <th width="150px">TOTAL ATTENDANCE</th>
                <th width="50px"></th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
      </div>

    </section>

<script type="text/javascript">
  $(document).ready( function(){
    events_data();
  });

  function events_data(){
    var tbl = $('#tbl_events').DataTable();
   tbl.destroy();
    $('#tbl_events').DataTable({
        "processing": true,
        "ajax": {
            "url": "../ajax/datatables/eventboard.php",
            "type": "POST"
        },
        "columns": [
          { "data": "count" },
          { "data": "event_date" },
          { "data": "event_name" },
          { "data": "event_status" },
          { "data": "event_attendance" },
          {
            "mRender": function(data,type,row){
              var disable = row.disable == 1?"disabled":"";
              var x = "<?php echo $_SESSION['role']?>"=="Administrator"?"disabled":"";
              return "<button class='btn btn-primary btn-sm' "+x+" "+disable+" onclick='attendance("+row.event_id+")'>Attendance</button>";
            }
          }
        ],
        "createdRow": function ( row, data, dataIndex ) {
            if (data.alert == 1) {
                $(row).css("background-color","#f5dfa2");
            }
        }
    });
  }

  function attendance(eID){
    window.location="index.php?page=<?php echo page_url('attendance')?>&event_id="+eID;
  }

</script>
