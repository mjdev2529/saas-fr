<?php

  $type = $_GET["type"];

  $attendee = $type == "S"?"STUDENTS":"VISITORS";
  $hide = $type == "S"?"":"display:none;";
  $hide1 = $type == "V"?"":"display:none;";
?>
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Attendees</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?php echo page_url('dashboard')?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Attendees</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <div class="card">
        <div class="card-header border-transparent">
          <h3 class="card-title">ATTENDEES ( <b><?php echo $attendee;?></b> )</h3>
        </div>
        <!-- /.card-header -->
        <div class="col-md-12 row">
          <div class="btn-group offset-md-11" style="<?=$hide?>">
            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#md_attendees_student">Add</button>
            <button class="btn btn-sm btn-danger" onclick="delete_student()">Delete</button>
          </div>
          <div class="btn-group offset-md-11" style="<?=$hide1?>">
            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#md_attendees_visitor">Add</button>
            <button class="btn btn-sm btn-danger" onclick="delete_visitor()">Delete</button>
          </div>
        </div>
        <div class="card-body" style="<?=$hide?>">
          <div class="table-responsive">
            <table id="tbl_students" width="100%" class="table table-bordered">
              <thead>
              <tr class="bg-dark">
                <th width="10px"><input type="checkbox" id="checkAllStudents" value="0" onclick="checkAllStudents()"></th>
                <th width="10px">#</th>
                <th width="10px"></th>
                <th width="100px">STUDENT ID</th>
                <th>NAME</th>
                <th width="120px">COURSE</th>
                <th width="140px">YEAR AND SECTION</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>

        <div class="card-body" style="<?=$hide1?>">
          <div class="table-responsive">
            <table id="tbl_visitors" class="table table-bordered">
              <thead>
              <tr class="bg-dark">
                <th width="10px"><input type="checkbox" id="checkAllVisitors" value="0" onclick="checkAll_Visitors()"></th>
                <th width="10px">#</th>
                <th width="10px"></th>
                <th>NAME</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>

      </div>

    </section>

<?php
  include "../modals/attendees_student_add.php";
  include "../modals/attendees_student_edit.php";
  include "../modals/attendees_visitor_add.php";
  include "../modals/attendees_visitor_edit.php";
?>


<script type="text/javascript">
  $(document).ready( function(){
    visitors_data();
    <?php if($_GET["type"] == "S"){?>
      students_data();
    <?php }?>
  });

  function students_data(){
    var tbl = $('#tbl_students').DataTable();
   tbl.destroy();
    $('#tbl_students').DataTable({
        "processing": true,
        "ajax": {
            "url": "../ajax/datatables/attendees_students.php",
            "type": "POST",
            "data": {type: "S"}
        },
        "columns": [
          {
            "mRender": function(data,type,row){
              return "<input type='checkbox' value='"+row.attendee_id+"' name='students'>";
            }
          },
          { "data": "count" },
          {
            "mRender": function(data,type,row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='edit_student("+row.attendee_id+")'><i class='fa fa-edit'></i></button>";
            }
          },
          { "data": "s_id" },
          { "data": "attendee_name" },
          { "data": "course" },
          { "data": "year_sec" }
        ]
    });
  }

   function visitors_data(){
    var tbl = $('#tbl_visitors').DataTable();
   tbl.destroy();
    $('#tbl_visitors').DataTable({
        "processing": true,
        "ajax": {
            "url": "../ajax/datatables/attendees_students.php",
            "type": "POST",
            "data": {type: "V"}
        },
        "columns": [
          {
              "mRender": function(data,type,row){
                return "<input type='checkbox' value='"+row.attendee_id+"' name='visitors'>";
              }
            },
            { "data": "count" },
            {
              "mRender": function(data,type,row){
                return "<button class='btn btn-sm btn-outline-dark' onclick='edit_visitor("+row.attendee_id+")'><i class='fa fa-edit'></i></button>";
              }
            },
             { "data": "visitors_name" }
        ]
    });
  }

  // S T U D E N T S 

  $("#studentAdd_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/student_add.php";
    $.ajax({
      "type":"POST",
      "url":url,
      "data":data,
      success: function(data){
        if(data == 1){
          alert("New Student was added successfully!");
          $("#md_attendees_student").modal("hide");
          students_data();
        }else{
          alert("Error!"+data);
        }
      }
    });
  });

  function edit_student(sID){
    var url = '../ajax/get_student_data.php'
    $.ajax({
      "type":"POST",
      "url":url,
      "data":{sID: sID, type: "S"},
      success: function(data){
        $("#md_student_edit").modal();
        var o = JSON.parse(data);
        $("#a_id").val(o.attendee_id);
        $("#edit_s_id_num").val(o.student_id);
        $("#edit_s_fname").val(o.fname);
        $("#edit_s_lname").val(o.lname);
        $("#edit_s_course").val(o.course_id);
        $("#edit_s_year").val(o.student_year);
        $("#edit_s_section").val(o.student_section);
        $("#edit_s_contact_num").val(o.contact_num);
        $("#edit_s_address").html(o.address);
      }
    });
  }

  $("#studentEdit_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/student_update.php";
    $.ajax({
      "type":"POST",
      "url":url,
      "data":data,
      success: function(data){
        if(data == 1){
          alert("Student data was updated successfully!");
          $("#md_student_edit").modal("hide");
          students_data();
        }else{
          alert("Error!"+data);
        }
      }
    });
  });

  function checkAllStudents(){
    var x = $("#checkAllStudents").is(":checked");
    if(x){
      $("input[name=students]").prop("checked", true);
    }else{
      $("input[name=students]").prop("checked", false);
    }
  }

  function delete_student(){
    var data = [];
    var url = "../ajax/student_delete.php";
    $("input[name=students]:checked").each( function(){
      data.push($(this).val());
    });
    var x = confirm("Are you sure to delete?");
    if(x){
      if(data != ""){
        $.ajax({
          "type":"POST",
          "url":url,
          "data":{data: data},
          success: function(data){
            if(data == 1){
              alert("Selected data was deleted successfully!");
              students_data();
            }else{
              alert("Error!"+data);
            }
          }
        })
      }else{
        alert("No data selected.");
      }
    }
  }

  // S T U D E N T S  E N D

  // V I S I T O R S

  $("#visitorAdd_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/visitor_add.php";
    $.ajax({
      "type":"POST",
      "url":url,
      "data":data,
      success: function(data){
        if(data == 1){
          alert("New Visitor was added successfully!");
          $("#md_attendees_visitor").modal("hide");
          visitors_data();
        }else{
          alert("Error!"+data);
        }
      }
    });
  });

  function edit_visitor(sID){
    var url = '../ajax/get_student_data.php'
    $.ajax({
      "type":"POST",
      "url":url,
      "data":{sID: sID, type: "V"},
      success: function(data){
        $("#md_visitor_edit").modal();
        var o = JSON.parse(data);
        $("#a_Vid").val(o.attendee_id);
        $("#edit_fname").val(o.fname);
        $("#edit_lname").val(o.lname);
        $("#edit_contact_num").val(o.contact_num);
        $("#edit_address").html(o.address);
      }
    });
  }

  $("#visitorEdit_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/visitor_update.php";
    $.ajax({
      "type":"POST",
      "url":url,
      "data":data,
      success: function(data){
        if(data == 1){
          alert("Visitor data was updated successfully!");
          $("#md_visitor_edit").modal("hide");
          visitors_data();
        }else{
          alert("Error!"+data);
        }
      }
    });
  });

  function checkAll_Visitors(){
    var x = $("#checkAllVisitors").is(":checked");
    if(x){
      $("input[name=visitors]").prop("checked", true);
    }else{
      $("input[name=visitors]").prop("checked", false);
    }
  }

  function delete_visitor(){
    var data = [];
    var url = "../ajax/visitor_delete.php";
    $("input[name=visitors]:checked").each( function(){
      data.push($(this).val());
    });
    var x = confirm("Are you sure to delete?");
    if(x){
      if(data != ""){
        $.ajax({
          "type":"POST",
          "url":url,
          "data":{data: data},
          success: function(data){
            if(data == 1){
              alert("Selected data was deleted successfully!");
              visitors_data();
            }else{
              alert("Error!"+data);
            }
          }
        })
      }else{
        alert("No data selected.");
      }
    }
  }

</script>