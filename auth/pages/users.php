<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Manage Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active">Manage Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <div class="card">
              <div class="card-header border-transparent">
                <div class="btn-group float-sm-right">
                  <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_user_md">Add</button>
                  <button class="btn btn-sm btn-danger" onclick="delete_user()">Delete</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tbl_users" class="table table-bordered">
                    <thead class="bg-dark">
                    <tr>
                      <th width="5px"><input type="checkbox" id="cb_users" onclick="checkAllUsers()"></th>
                      <th width="5px">#</th>
                      <th width="5px" ></th>
                      <th>NAME</th>
                      <th width="100px">ROLE</th>
                      <th width="50px">STATUS</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
            </div>

    </section>

    <?php 
      include '../modals/add_user.php';
      include '../modals/edit_user.php';
     ?>

    <!-- /.content -->
<script type="text/javascript">
  $(document).ready(function() {
    user_data();
  });

  function user_data(){
   var tbl = $('#tbl_users').DataTable();
   tbl.destroy();
    $('#tbl_users').DataTable( {
        "processing": true,
        "ajax": {
            "url": "../ajax/datatables/users.php",
            "type": "POST"
        },
        "columns": [
            { 
                "mRender": function(data,type,row){
                  return "<input type='checkbox' name='cb_users' value='"+row.u_id+"'>";
              } 
            },
            { "data": "count" },
            { 
                "mRender": function(data,type,row){
                  return "<button class='btn btn-sm btn-dark' onclick='edit_MD("+row.u_id+")'><i class='fa fa-list'></i></button>";
              } 
            },
            { "data": "uname" },
            { "data": "urole" },
            { "data": "ustat" }
        ]
    });
  }

  $("#addUser_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/add_user.php"

    $.ajax({
      "type": "POST",
      "url": url,
      "data": data,
      "success": function(data){
        if(data == 1){
          alert("User was added successfully.");
          $("#add_user_md").modal("hide");
          user_data();
        }else{
          alert("Error!");
        }
      }
    });
  });

  function checkAllUsers(){
    var x = $("#cb_users").is(":checked");
    if(x){
      $("input[name=cb_users]").prop("checked", true);
    }else{
      $("input[name=cb_users]").prop("checked", false);
    }
  }

  function delete_user(){
    var url = "../ajax/delete_user.php"
    var x = [];
    $("input[name=cb_users]:checked").each( function(){
      x.push($(this).val());
    });
    
    if(x != "")
    {
      var z = confirm("Are you sure to delete selected user/s?");
      if(z){
        $.ajax({
          "type": "POST",
          "url": url,
          "data": {u_id: x},
          "success": function(data){
            if(data == 1){
              alert("Selected user/s successfully deleted.");
              user_data();
            }else{
              alert("Error!");
            }
          }
        });
      }
    }else{
      alert("No data was selected!");
    }
  }

  function edit_MD(uID){
    $("#edit_user_md").modal();
    user_details(uID);
  }

  function user_details(uID){
    var url = "../ajax/user_data.php"
    $.ajax({
      "type": "POST",
      "url": url,
      "data": {uID: uID},
      "success": function(data){
        if(data){
          var o = JSON.parse(data);
          $("#e_ufname").val(o.u_fname);
          $("#e_umname").val(o.u_mname);
          $("#e_ulname").val(o.u_lname);
          $("#e_uname").val(o.uname);
          $("#e_upass").val(o.password);
          $("#e_role").val(o.urole);
          $("#e_stat").val(o.ustatus);
          $("#u_id").val(o.user_id);
        }
      }
    });
  }

  $("#editUser_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/edit_user.php"

    $.ajax({
      "type": "POST",
      "url": url,
      "data": data,
      "success": function(data){
        if(data == 1){
          alert("User details was updated successfully.");
          $("#edit_user_md").modal("hide");
          user_data();
        }else{
          alert("Error!");
        }
      }
    });
  });

</script>