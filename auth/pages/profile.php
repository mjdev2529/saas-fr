<?php

  $user_id = $_SESSION['user_id'];
  $user_data = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$user_id'"));

?>
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?php echo page_url('dashboard')?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <div class="card">
        <div class="card-header border-transparent">
          <h3 class="card-title">Profile of ( <b><?php echo $_SESSION['user'];?></b> )</h3>
        </div>
        <!-- /.card-header -->
        <div class="col-md-12">
          <form method="post" action="" id="user_profile_form" class="col-md-12">
            <input type="hidden" value="<?=$_SESSION['user_id']?>" name="user_id">
            <div class="card-body row">
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">First name</label>
                <input type="text" class="form-control" name="fname" placeholder="First Name" value="<?=$user_data['u_fname']?>">
              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">User role</label>
                <input type="text" class="form-control" value="<?=$_SESSION['role']?>" disabled>
              </div>
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="exampleInputPassword1">Middle name</label>
                <input type="text" class="form-control" name="mname" placeholder="Middle Name" value="<?=$user_data['u_mname']?>">
              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Phone #</label>
                <input type="text" class="form-control" name="phone" maxlength="11" placeholder="Phone Number" value="<?=$user_data['phone_num']?>">
              </div>
              <div class="col-md-4"></div>
              <div class="form-group col-md-4">
                <label for="exampleInputPassword1">Last name</label>
                <input type="text" class="form-control" name="lname" placeholder="Last Name" value="<?=$user_data['u_lname']?>">
              </div>
              <div class="col-md-8"></div>
              <div class="form-group col-md-8">
                <label for="exampleInputEmail1">Address</label>
                <textarea class="form-control" name="address"><?=$user_data['address']?></textarea>
              </div>
              <hr>
              <h5 class="col-md-12">Update password</h5>
              <br>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">New password</label>
                <input type="password" name="new_pass" class="form-control" placeholder="New Password">
              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Confirm password</label>
                <input type="password" name="new_pass1" class="form-control" placeholder="Confirm Password">
              </div>
              <hr>
              <h5 class="col-md-12"><b>[ REQUIRED! ]</b></h5>
              <br>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Password</label>
                <input type="password" name="pass" class="form-control" placeholder="Password" oninput="pass_checker()">
                <small class="text-muted">Input PASSWORD to save changes</small>
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-success float-sm-right" disabled=""><i class="fa fa-save"></i> Save Changes</button>
            </div>
          </form>
        </div>

      </div>

    </section>


<script type="text/javascript">
  $(document).ready( function(){
  });

  function pass_checker(){
    var x = $("input[name=pass]").val();
    if(x){
      $(".btn-success").prop("disabled", false);
    }else{
      $(".btn-success").prop("disabled", true);
    }
  }

  $("#user_profile_form").submit( function(e){
    e.preventDefault();
    var url = "../ajax/user_profile_update.php";
    var data = $(this).serialize();
    var pass = $("input[name=new_pass]").val();
    var pass1 = $("input[name=new_pass1]").val();
    if(pass != "" && pass1 != "" || pass == pass1){
      $.ajax({
        url:url,
        data: data,
        type: "POST",
        success: function(data){
          if(data == 1){
            alert("Profile updated successfully!");
            window.location.reload();
          }else if(data == 2){
            alert("Entered password incorrect!");
          }else{
            alert("Error: "+data);
          }
        }
      });
    }else{
      alert("New password doesn't match!");
    }

  });

</script>