<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Course</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?=page_url('dashboard')?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Course</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <div class="card">
        <div class="card-header border-transparent">
          <h3 class="card-title">COURSE</h3>
        </div>
        <!-- /.card-header -->
        <div class="col-md-12">
          <div class="btn-group col-sm-1 offset-sm-11">
            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#md_course_add">Add</button>
            <button class="btn btn-sm btn-danger" onclick="course_delete()">Delete</button>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="tbl_course" class="table table-bordered">
              <thead>
              <tr class="bg-dark">
                <th width="10px"><input type="checkbox" id="checkAll" onclick="checkAll()"></th>
                <th width="10px">#</th>
                <th width="10px"></th>
                <th width="">COURSE NAME</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
      </div>

    </section>
<?php 
  include "../modals/md_course_edit.php";
  include "../modals/md_course_add.php";
?>

<script type="text/javascript">
  $(document).ready( function(){
    courses_data();
  });

  function courses_data(){
    var tbl = $('#tbl_course').DataTable();
   tbl.destroy();
    $('#tbl_course').DataTable({
        "processing": true,
        "ajax": {
            "url": "../ajax/datatables/course_data_dt.php",
            "type": "POST"
        },
        "columns": [
          { 
            "mRender": function(data,type,row){
              return "<input type='checkbox' value='"+row.course_id+"' name='cb_course'>";
            }
          },
          { "data": "count" },
          { 
            "mRender": function(data,type,row){
              return "<button class='btn btn-sm btn-outline-dark' onclick='courseEdit("+row.course_id+")'><i class='fa fa-edit'></i></button>";
            }
          },
          { "data": "course_name" }
        ]
    });
  }

  function checkAll(){
    var x = $("#checkAll").is(":checked");
    if(x){
      $("input[name=cb_course]").prop("checked", true);
    }else{
      $("input[name=cb_course]").prop("checked", false);
    }
  }

  $("#courseAdd_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/course_add.php";
    $.ajax({
      "type":"POST",
      "url": url,
      "data": data,
      success: function(data){
        if(data == 1){
          alert("New Course was added successfully.");
          courses_data();
          $("#md_course_add").modal("hide");
          $("input").val("");
        }else{
          alert("Error!"+data);
        }
      }
    });

  });

  function course_delete(){
    var x = [];
    $("input[name=cb_course]:checked").each( function(){
      x.push($(this).val());
    });
    var url = "../ajax/course_delete.php";
    if(x != ""){
      var a = confirm("Are you sure to delete?");
      if(a){
        $.ajax({
          "type":"POST",
          "url": url,
          "data": {cID:x},
          success: function(data){
            if(data == 1){
              alert("Selected Course was deleted successfully.");
              courses_data();
            }else{
              alert("Error!"+data);
            }
          }
        });
      }
    }else{
      alert("No Data Selected!");
    }
  }


  function courseEdit(cID){
    $("#md_course_edit").modal();
    getCourseData(cID);
  }

  function getCourseData(cID){
    var url = "../ajax/course_data.php";
    $.ajax({
      "type":"POST",
      "url": url,
      "data": {cID: cID},
      success: function(data){
        var o = JSON.parse(data);
        $("#cname").val(o.course_name);
        $("#c_id").val(o.course_id);
      }
    });
  }

  $("#courseEdit_form").submit( function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var url = "../ajax/course_edit.php";
    $.ajax({
      "type":"POST",
      "url": url,
      "data": data,
      success: function(data){
        if(data == 1){
          alert("New Course was updated successfully.");
          courses_data();
          $("#md_course_edit").modal("hide");
          $("input").val("");
        }else{
          alert("Error!"+data);
        }
      }
    });

  });

</script>
