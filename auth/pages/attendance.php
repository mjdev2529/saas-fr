<?php
  
  $event_id = $_GET["event_id"];
  $row = mysql_fetch_array(mysql_query("SELECT * FROM tbl_events_activities WHERE event_id = '$event_id'"))

?>
<style type="text/css">
  .lds-ring {
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 64px;
    height: 64px;
    margin: 8px;
    border: 8px solid #fff;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #000 transparent transparent transparent;
  }
  .lds-ring div:nth-child(1) {
    animation-delay: -0.45s;
  }
  .lds-ring div:nth-child(2) {
    animation-delay: -0.3s;
  }
  .lds-ring div:nth-child(3) {
    animation-delay: -0.15s;
  }
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
</style>
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Attendance</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?page=<?php echo page_url('dashboard')?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Attendance</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header border-transparent pb-2">
          <h4 class="">ATTENDANCE FOR <u><?php echo strtoupper($row["event_name"]);?></u></h4>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0">
          <div class="row">
            <div class="col-md-8" style="border: 1px solid; height: 510px;">
              <div class="col-md-12" style="border: 1px solid; height: 430px; margin-top: 5px;  margin-bottom: 5px;">
                <center><h1>PHOTO CAPTURE HERE</h1></center>
              </div>
              <button id="btn-capture" class="btn btn-lg btn-success btn-block" onclick="capture()"><i class="fa fa-camera"></i> CAPTURE</button>
              
            </div>
            <div class="col-md-4" style="border: 1px solid; height: 510px;">

              <div id="default_wrap" style="margin-top: 50%; /*display: none;*/">
                <center>
                  <h4 style="color: gray;">No Data Available.</h4>
                </center>
              </div>

              <div id="loader_wrap" style="margin-top: 40%; display: none;">
                <center>
                  <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                  <h4>Loading Data, Please Wait...</h4>
                </center>
              </div>

              <div id="data_wrap" class="col-md-12 pt-2" style="display: none;">
                <div id="data_true">
                  <h3>Personal Details</h3>
                  <hr>
                  <dl class="row">
                    <dt class="col-sm-3">Name:</dt>
                    <dd class="col-sm-9">Tests</dd>

                    <dt class="col-sm-3">Course:</dt>
                    <dd class="col-sm-9">BSINFO</dd>

                    <dt class="col-sm-3">Year:</dt>
                    <dd class="col-sm-9">II</dd>

                    <dt class="col-sm-3">Section:</dt>
                    <dd class="col-sm-9">A</dd>
                  </dl>
                  <button class="btn btn-primary btn-block btn-lg"><i class="fa fa-check-circle"></i> CONFIRM</button>
                  <button class="btn btn-danger btn-block btn-lg" onclick="retry()"><i class="fa fa-undo-alt"></i> RETRY</button>
                </div>

                <div id="data_false" style="margin-top: 50%;">
                  <h3 class="text-center mb-5">No Personal Details Available.</h3>
                  <button class="btn btn-primary btn-block btn-lg" onclick="add_md()"><i class="fa fa-plus-circle"></i> ADD</button>
                </div>

              </div>


            </div>
          </div>
        </div>
      </div>
    </section>
<?php
  include "../modals/event_attendance_add.php";
?>
<script type="text/javascript">
  function capture(){
    $("#btn-capture").prop("disabled",true);
    $("#default_wrap").hide();
    $("#loader_wrap").show();
    setTimeout( function(){
      $("#loader_wrap").hide();
      $("#data_wrap").show();
      $("#data_true").hide();
    },3000);
  }

  function retry(){
    $("#btn-capture").prop("disabled",false);
    $("#data_wrap").hide();
    $("#default_wrap").show();
  }

  function add_md(){
    $("#md_event_attendance").modal();
  }

  function attendee_type(){
    var x = $("#attendee_type").val();
    if(x == 1){
      $("#studentAdd_form").show();
      $("#visitorAdd_form").hide();
    }else{
      $("#studentAdd_form").hide();
      $("#visitorAdd_form").show();
    }
  }

  $("#studentAdd_form").submit( function(e){
    e.preventDefault();
    var a_type = $("#attendee_type").val();
    var data = $(this).serialize()+"&a_type="+a_type;
    alert(data);
  });

  $("#visitorAdd_form").submit( function(e){
    e.preventDefault();
    var a_type = $("#attendee_type").val();
    var data = $(this).serialize()+"&a_type="+a_type;
    alert(data);
  });
</script>
