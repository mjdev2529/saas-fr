<?php

	$pages = $_GET["page"];
	if($pages == page_url("dashboard")){
		require '../auth/pages/dashboard.php';
	}else if($pages == page_url("events")){
		require '../auth/pages/events.php';
	}else if($pages == page_url("users")){
		require '../auth/pages/users.php';
	}else if($pages == page_url("attendance")){
		require '../auth/pages/attendance.php';
	}else if($pages == page_url("attendees")){
		require '../auth/pages/attendees.php';
	}else if($pages == page_url("course")){
		require '../auth/pages/course.php';
	}else if($pages == page_url("profile")){
		require '../auth/pages/profile.php';
	}else{
		require '../auth/pages/404.php';
	}

?>