<?php

	include '../../core/config.php';

	$sql = mysql_query("SELECT * FROM tbl_users WHERE ustatus != '-1' AND urole != '-1'");

	$count = 1;
	$response['data'] = array();
	while($row = mysql_fetch_array($sql)){
		$list = array();

		if($row["urole"] == 1){
			$urole = "Committee";
		}else if($row["urole"] == 2){
			$urole = "Guard";
		}else{
			$urole = "Administrator";
		}

		if($row["ustatus"] == 1){
			$ustat = "<span class='badge badge-success'>Active</span>";
		}else{
			$ustat = "<span class='badge badge-secondary'>Disabled</span>";
		}

		$list['u_id'] = $row["user_id"];
		$list['count'] = $count++;
		$list['uname'] = $row["u_lname"]." ".$row["u_fname"].", ".$row["u_mname"];
		$list['urole'] = $urole;
		$list['ustat'] = $ustat;

		array_push($response['data'],$list);
	}

	echo json_encode($response);

?>