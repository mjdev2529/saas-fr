<?php

	include '../../core/config.php';

	$type = isset($_POST["type"]) && $_POST["type"] != "S"?"V":"S";
	$sql = mysql_query("SELECT * FROM tbl_attendees WHERE attendee_type = '$type'");
	date_default_timezone_set('Asia/Manila');

	$count = 1;
	$response['data'] = array();
	while($row = mysql_fetch_array($sql)){
		$list = array();

		$list['attendee_id'] = $row["attendee_id"];
		$list['count'] = $count++;
		$list['attendee_name'] = $row['lname'].", ".$row['fname'];
		if($type == "S"){
			$list['s_id'] = $row['student_id'];
			$list['course'] = get_course($row['course_id']);
			$list['year_sec'] = get_year_sec($row["student_year"],$row["student_section"]);
		}
		$list['visitors_name'] = $type == "V"? $row['lname'].", ".$row['fname']:"";

		array_push($response['data'],$list);
	}

	echo json_encode($response);

	function get_course($cID){
		$course = mysql_fetch_array(mysql_query("SELECT course_name FROM tbl_course WHERE course_id = '$cID'"));
		return $course[0];
	}

	function get_year_sec($s_year,$s_section){
		if($s_year == 1){
			$syear = "I - ";
		}else if($s_year == 2){
			$syear = "II - ";
		}else if($s_year == 3){
			$syear = "III - ";
		}else if($s_year == 4){
			$syear = "IV - ";
		}else if($s_year == 5){
			$syear = "V - ";
		}else{
			$syear = "IRREGULAR";
		}

		$s_section = $syear == "IRREGULAR"?"":$s_section;

		return $syear.$s_section;
	}

?>