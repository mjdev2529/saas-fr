<?php

	include '../../core/config.php';

	$sql = mysql_query("SELECT * FROM tbl_events_activities");
	date_default_timezone_set('Asia/Manila');

	$count = 1;
	$response['data'] = array();
	while($row = mysql_fetch_array($sql)){
		$list = array();

		$edate = date("Y-m-d", strtotime($row["event_date"]));
		$now = date("Y-m-d");
		$event_end = date("Y-m-d H:i:s", strtotime($row["event_date"]." ".$row["event_time1"]));
		$get_time = date("Y-m-d H:i:s");
		$alert = 0;

			if($row['event_status'] == 1){
				$estat = "<span class='badge badge-success'>Finished</span>";
			}else if($row['event_status'] == 2){
				$estat = "<span class='badge badge-danger'>Canceled</span>";
			}else{
				if($edate > $now){
					$estat = "<span class='badge badge-warning'>Upcoming</span>";
				}else{
					if($event_end < $get_time){
						$alert = 1;
						$estat = "<span class='badge badge-danger'>Expired</span>";
					}else{
						$estat = "<span class='badge badge-primary'>Ongoing</span>";
					}
				}
			}

		$list['event_id'] = $row["event_id"];
		$list['count'] = $count++;
		$list['event_name'] = $row['event_name'];
		$list['event_date'] = $edate;
		$list['event_status'] = $estat;
		$list['event_attendance'] = 0;
		$list['alert'] = $alert;
		$list['duration'] = " [ ".date("H:i A", strtotime($row["event_time"]))." - ".date("h:i A", strtotime($row["event_time1"]))." ]";
		$list['disable'] = $row['event_status'] == 1 || $alert == 1?1:0;

		array_push($response['data'],$list);
	}

	echo json_encode($response);

?>