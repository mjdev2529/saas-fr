<script type="text/javascript">
	$("#addUser_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var url = "ajax/add_user.php";
		$("#add_user_md").modal("hide");
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			success: function(data){
				if(data == 1){
					alert("User was successfully added.");
					$("input").val("");
					$("select").val("0");
					getUsers();
				}
			}
		});
	});

	function delete_user(){
	var con = confirm("Are you sure to delete selected?");

	if(con)
	{
		var check = $("input[name=u_id]").is(":checked");
	    var x = [];
	    $("input[name=u_id]:checked").each( function(){
	      x.push($(this).val());
	    });

	    if(check){
	    	$.ajax({
	    		type: "POST",
	    		url: "ajax/delete_user.php",
	    		data: {uID: x},
	    		success: function(data){
	    			if(data == 1){
	    				alert("Selected user was deleted.");
	    				getUsers();
	    			}else{
	    				alert("Error!");
	    			}
	    		}
	    	});
	    }else{
	    	alert("Please select user first!");
	    }
	}
  }

  function checkAllUser(){
    var cb = $("#cb_users").is(":checked");

    if(cb){
      $("input[name=u_id]").prop("checked", true);
    }else{
      $("input[name=u_id]").prop("checked", false);
    }
  }

  function checkAllData(){
    var cb = $("#cb_events").is(":checked");

    if(cb){
      $("input[name=e_id]").prop("checked", true);
    }else{
      $("input[name=e_id]").prop("checked", false);
    }
  }

  function edit_user(id){
  	$("#edit_user_md").modal();
  	getUserData(id);
  	$("#u_id").val(id);
  }

  function getUserData(id){
  	$.ajax({
  		type: "POST",
  		url: "ajax/user_data.php",
  		data: {uID: id},
  		success: function(data){
  			var o = JSON.parse(data);
  			$("#e_ufname").val(o.user_name);
  			$("#e_umname").val(o.user_middle_name);
  			$("#e_ulname").val(o.user_last_name);
  			$("#e_uname").val(o.username);
  			$("#e_upass").val(o.password);
  			$("#e_role").val(o.user_status);
  		}
  	});
  }

  $("#editUser_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var url = "ajax/edit_user.php";
		$("#edit_user_md").modal("hide");
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			success: function(data){
				if(data == 1){
					alert("User data was successfully updated.");
					$("input").val("");
					$("select").val("0");
					getUsers();
				}
			}
		});
	});

  	$("#addEvent_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var url = "ajax/add_event.php";
		$("#add_event_md").modal("hide");
		var aud = $("#audience").val();

		if(aud != -1){
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function(data){
					if(data == 1){
						alert("Event was successfully added.");
						$("input").val("");
						$("textarea").html("");
						$("select").val("-1");
						getEvents();
					}
				}
			});
		}else{
			alert("Please select an audience.");
		}
	});

	function delete_event(){
	var con = confirm("Are you sure to delete selected?");

	if(con)
	{
		var check = $("input[name=e_id]").is(":checked");
	    var x = [];
	    $("input[name=e_id]:checked").each( function(){
	      x.push($(this).val());
	    });

	    if(check){
	    	$.ajax({
	    		type: "POST",
	    		url: "ajax/delete_event.php",
	    		data: {uID: x},
	    		success: function(data){
	    			if(data == 1){
	    				alert("Selected event was deleted.");
	    				getEvents();
	    			}else{
	    				alert("Error!");
	    			}
	    		}
	    	});
	    }else{
	    	alert("Please select event first!");
	    }
	}
  }

   function edit_event(id){
  	$("#edit_event_md").modal();
  	getEventData(id);
  	$("#e_id").val(id);
  }

  function getEventData(id){
  	$.ajax({
  		type: "POST",
  		url: "ajax/event_data.php",
  		data: {uID: id},
  		success: function(data){
  			var o = JSON.parse(data);
  			$("#e_id").val(o.event_id);
  			$("#e_title").val(o.title);
  			$("#e_content").html(o.content);
  			$("#e_audience").val(o.audience);
  		}
  	});
  }

  $("#editEvent_form").submit( function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var url = "ajax/edit_event.php";
		$("#edit_event_md").modal("hide");
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			success: function(data){
				if(data == 1){
					alert("Event data was successfully updated.");
					$("input").val("");
					$("select").val("0");
					getEvents();
				}
			}
		});
	});

</script>